/**
 * @file
 * ES6 module syntax fallback loader.
 */

((window, document) => {
  window._dsLoad = (src) => {
    const script = document.createElement('script');
    script.defer = true;
    script.src = src;

    const scripts = document.getElementsByTagName('script');
    const last = scripts[scripts.length - 1];
    last.parentNode.insertBefore(script, last);
  };
})(window, document);
