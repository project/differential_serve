<?php

namespace Drupal\differential_serve;

/**
 * JavaScript asset group definitions.
 */
class JsGroups {

  /**
   * Group for JavaScript assets targeting browsers that support ES6 modules.
   */
  const MODERN = 10;

  /**
   * Group for JavaScript targeting browsers that do not support ES6 modules.
   */
  const LEGACY = 11;

}
