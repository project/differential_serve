<?php

namespace Drupal\differential_serve\Asset;

use Drupal\Core\Asset\AssetCollectionRendererInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;

/**
 * Modifies JavaScript HTML tags for differential serving.
 */
class DifferentialServeJsCollectionRenderer implements AssetCollectionRendererInterface {

  /**
   * The JS asset collection renderer service.
   *
   * @var \Drupal\Core\Asset\AssetCollectionRendererInterface
   */
  protected $inner;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * Constructs a DifferentialServeJsCollectionRenderer object.
   *
   * @param \Drupal\Core\Asset\AssetCollectionRendererInterface $js_collection_renderer
   *   The JS asset collection renderer service.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   */
  public function __construct(AssetCollectionRendererInterface $js_collection_renderer, FileUrlGeneratorInterface $file_url_generator) {
    $this->inner = $js_collection_renderer;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public function render(array $js_assets) {
    $elements = $this->inner->render($js_assets);

    foreach (array_filter($js_assets, [static::class, 'isDifferentiallyServed']) as $js_asset) {
      $src = $this->fileUrlGenerator->generateString($js_asset['data']);

      foreach ($elements as $key => $element) {
        if (isset($element['#attributes']['src']) && strpos($element['#attributes']['src'], $src) === 0) {
          if ($js_asset['differential_serve'] == 'modern') {
            $elements[$key] = static::processModernScript($element);
          }
          elseif ($js_asset['differential_serve'] == 'legacy') {
            $elements[$key] = static::processLegacyScript($element);
          }
        }
      }
    }

    return $elements;
  }

  /**
   * Returns whether a given JavaScript asset is to be differentially served.
   *
   * @param array $js_asset
   *   The JavaScript asset definition.
   *
   * @return bool
   *   Returns TRUE for a JavaScript asset that is to be differentially served,
   *   FALSE otherwise.
   */
  protected static function isDifferentiallyServed(array $js_asset) {
    return $js_asset['type'] == 'file' && isset($js_asset['differential_serve']);
  }

  /**
   * Processes a script render array as a modern script.
   *
   * @param array $element
   *   The modern script render array to process.
   *
   * @return array
   *   Processed element.
   */
  protected static function processModernScript(array $element) {
    $element['#attributes']['type'] = 'module';
    return $element;
  }

  /**
   * Processes a script render array as a legacy script.
   *
   * @param array $element
   *   The legacy script render array to process.
   *
   * @return array
   *   Processed element.
   */
  protected static function processLegacyScript(array $element) {
    $element['#attributes']['nomodule'] = TRUE;
    $element['#value'] = 'window._ESM||window._dsLoad("' . $element['#attributes']['src'] . '")';
    unset($element['#attributes']['src']);
    return $element;
  }

}
