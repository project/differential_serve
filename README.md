INTRODUCTION
------------

Adds the ability to [differentially serve] JavaScript on the basis of whether
the current browser supports ES6 modules or not. This allows users to pay less
of a penalty for when a site needs to support older browsers.

There is some extra wrangling to support edge cases where a browser may not
respect the `nomodule` attribute and would then potentially execute more scripts
than desired.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.


USAGE
-----

In `extension.libraries.yml`:

```yml
scripts:
  js:
    modern.js: { differential_serve: 'modern' } # ES6 only.
    legacy.js: { differential_serve: 'legacy' } # ES5 only.
```

### GOTCHAS

Initial attached legacy scripts may not get their Drupal behavior `attach()`
methods called on the initial page load, due to the async nature of their
loading.


[differentially serve]: https://css-tricks.com/differential-serving/
