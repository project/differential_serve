<?php

namespace Drupal\Tests\differential_serve\Kernel;

use Drupal\Core\Render\HtmlResponse;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests mangling of JavaScript assets for differential loading.
 *
 * @group differential_serve
 */
class AssetMangleTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'differential_serve',
    'differential_serve_test',
    'system',
  ];

  /**
   * Tests alteration of modern JavaScript asset.
   */
  public function testModernScript() {
    $this->renderPageWithLibraries(['differential_serve_test/asset.modern']);

    $elements = $this->cssSelect('body > script[type="module"][src*="differential_serve_test/js/asset.modern.js"]');
    $this->assertCount(1, $elements, 'Attribute "type" set to "module".');
  }

  /**
   * Tests alteration of modern JavaScript assets with optimization.
   */
  public function testModernScriptOptimization() {
    $this->config('system.performance')->set('js.preprocess', TRUE)->save();
    $this->renderPageWithLibraries(['differential_serve_test/asset.modern_0']);

    $modern_scripts = $this->cssSelect('body > script[type="module"]');
    $this->assertCount(1, $modern_scripts, 'Optimization groups modern scripts.');

    $scripts = $this->cssSelect('body > script');
    $this->assertLessThan(count($scripts), count($modern_scripts), 'Other optimization groups are unaffected.');
  }

  /**
   * Tests alteration of legacy JavaScript asset.
   */
  public function testFallbackScript() {
    $this->renderPageWithLibraries(['differential_serve_test/asset.legacy']);

    $elements = $this->cssSelect('body > script:not([src])');
    $this->assertCount(1, $elements, 'No "src" attribute.');

    $script = reset($elements);
    $this->assertTrue(isset($script->attributes()['nomodule']), 'Has "nomodule" attribute set.');
    $this->assertMatchesRegularExpression('`window\._ESM||window\._dsLoad\(".+differential_serve_test/js/asset.legacy.js\b[^"]*"\)`', (string) $script, 'Script loaded inline.');

    $elements = $this->cssSelect('body > script[src*="differential_serve/js/loader.js?"]');
    $this->assertCount(1, $elements, 'Legacy loader script loaded.');
  }

  /**
   * Tests alteration of legacy JavaScript assets with optimization.
   */
  public function testFallbackScriptOptimization() {
    $this->config('system.performance')->set('js.preprocess', TRUE)->save();
    $this->renderPageWithLibraries(['differential_serve_test/asset.legacy_0']);

    $legacy_scripts = $this->cssSelect('body > script[nomodule]');
    $this->assertCount(1, $legacy_scripts, 'Optimization groups legacy scripts.');

    $scripts = $this->cssSelect('body > script');
    $this->assertLessThan(count($scripts), count($legacy_scripts), 'Other optimization groups are unaffected.');
  }

  /**
   * Render a page with the given libraries attached.
   *
   * @param string[] $libraries
   *   Libraries to attach to the page.
   */
  public function renderPageWithLibraries(array $libraries) {
    $build = [
      '#type' => 'html',
      'page' => [],
      '#attached' => ['library' => $libraries],
    ];

    $this->container->get('renderer')->renderRoot($build);

    $response = (new HtmlResponse())->setContent($build);
    $response = $this->container
      ->get('html_response.attachments_processor')
      ->processAttachments($response);

    $this->setRawContent($response->getContent());
  }

}
